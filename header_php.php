<?php

//Variables et définitions globales
require_once("lib/global_vars.php");

//Appel des librairies de fonctions
require_once("lib/functions_library.php");

//On démarre ou reprend une session sur le site
session_start();

//Test de connexion à la BDD
//IP, username, password, database
$bdd_mysql = new mysqli('localhost', 'root', '', 'ticket');

if ($bdd_mysql->connect_error) {
	//En cas d'erreur, on arrête le code PHP
	die('Erreur de connexion (' . $bdd_mysql->connect_errno . ') '
			. $bdd_mysql->connect_error);
}

//On vérifie si l'utilisateur est connecté
if (!isset($_SESSION['utilisateur_id'])) {
	//Je n'ai pas la session, je ne suis pas connecté
	
	//Je cherche à savoir si je suis dans la page login_ticket.php
	if (strpos($_SERVER['PHP_SELF'], "login_ticket.php") === FALSE) {
		//Changement de page
		header("Location: login_ticket.php");
		die; //On arrête le traitement php
	}
}







