<?php
	require_once("header_php.php");
?>

<?php
	if (isset($_POST['send'])) {
		//Je suis dans un formulaire car le bouton send a été envoyé
		
		//Déclaration des variables temporaires
		$post_login = "";
		$post_pwd = "";
		$post_pwd2 = "";
		
		//Assainir les données envoyées
		if (isset($_POST['login'])) {
			$post_login = addslashes(sanitize($_POST['login']));
		}
		if (isset($_POST['pwd'])) {
			//md5 = encrypter
			$post_pwd = md5(sanitize($_POST['pwd']));
		}
		if (isset($_POST['pwd2'])) {
			//md5 = encrypter
			$post_pwd2 = md5(sanitize($_POST['pwd2']));
		}
		
		//On vérifie que l'utilisateur existe dans la BDD
		$requete = "SELECT * FROM utilisateur WHERE login='".$post_login."'";
		//Exécution de la requête
		$resultat = $bdd_mysql->query($requete);
		
		if ($resultat->num_rows > 0) {
			//Il y a des résultats, on ne peut pas créer l'utilisateur
			
		} else {
			//Pas de résultat, le login est libre
			if ($post_pwd == $post_pwd2) {
				//Les mots de passe correspondent
				
				//Toutes les conditions sont OK, on peut créer le compte
				$requete = "INSERT INTO utilisateur
				(login, pwd)
				VALUES
				('".$post_login."','".$post_pwd."')";
				
				//Execution de la requête
				$resultat = $bdd_mysql->query($requete);
				
				//Dans la foulée, on se connecte
				//$bdd_mysql->insert_id
				//On met en mémoire l'ID utilisateur
				$_SESSION['utilisateur_id'] = $bdd_mysql->insert_id;
				
				//Changement de page
				header("Location: board_ticket.php");
				die; //On arrête le traitement php
			}
		}
	}

?>

<?php
	require_once("header_html.php");
?>
	
		<main>
			<h1>Inscription</h1>
			<form class="f_login" method="POST" action="">
				<div class="text_agauche">
					<label for="login">Login</label>
					<input type="text" id="login" name="login" placeholder="email@example.com" />
				</div>
				
				<div class="text_agauche">
					<label for="pwd">Password</label>
					<input type="password" id="pwd" name="pwd" placeholder="Password" />
				</div>
				
				<div class="text_agauche">
					<label for="pwd">Re-type Password</label>
					<input type="password" id="pwd2" name="pwd2" placeholder="Re-type Password" />
				</div>
				
				<div>
					<input type="submit" id="send" name="send" value="Send" />
				</div>
			</form>
		</main>
		
<?php
	require_once("footer_html.php");
?>


