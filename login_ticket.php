<?php
	require_once("header_php.php");
?>

<?php
	if (isset($_POST['send'])) {
		//Je suis dans un formulaire car le bouton send a été envoyé
		
		//Déclaration des variables temporaires
		$post_login = "";
		$post_pwd = "";
		
		//Assainir les données envoyées
		//$post_login = htmlspecialchars($_POST['login']);
		//$post_login = htmlentities($_POST['login']);
		//$post_login = strip_tags($_POST['login']);
		if (isset($_POST['login'])) {
			$post_login = sanitize($_POST['login']);
		}
		if (isset($_POST['pwd'])) {
			$post_pwd = sanitize($_POST['pwd']);
		}
		
		//On vérifie que l'utilisateur existe dans la BDD
		$requete = "SELECT * FROM utilisateur WHERE login='".$post_login."'";
		//$requete = "SELECT * FROM utilisateur WHERE login=\"".$post_login."\"";
		$resultat = $bdd_mysql->query($requete);
		
		//print_r($resultat);
		
		if ($resultat->num_rows > 0) {
			//Il y a des résultats
			$ligne_bdd = $resultat->fetch_array(MYSQLI_ASSOC);
			
			//print_r($ligne_bdd);
			
			if (md5($post_pwd) == $ligne_bdd['pwd']) {
				//Le mot de passe est le même
				
				//On met en mémoire l'ID utilisateur
				$_SESSION['utilisateur_id'] = $ligne_bdd['id'];
				
				//Changement de page
				header("Location: board_ticket.php");
				die(); //On arrête le traitement php
			}
		}
	}

?>

<?php
	require_once("header_html.php");
?>
	
		<main>
			<h1>Connexion</h1>
			<form class="f_login" method="POST" action="">
				<div class="text_agauche">
					<label for="login">Login</label>
					<input type="text" id="login" name="login" placeholder="email@example.com" />
				</div>
				
				<div class="text_agauche">
					<label for="pwd">Password</label>
					<input type="password" id="pwd" name="pwd" placeholder="Password" />
				</div>
				
				<div>
					<input type="submit" id="send" name="send" value="Send" />
				</div>
			</form>
		</main>
		
<?php
	require_once("footer_html.php");
?>


