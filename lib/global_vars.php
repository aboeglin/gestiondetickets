<?php

/* Etat des tickets
1 = Actif
0 = Archivé
-1 = Supprimé
2 = Résolu
*/

define('ETAT_TICKET_ACTIF', 1);
define('ETAT_TICKET_ARCHIVE', 0);
define('ETAT_TICKET_SUPPRIME', -1);
define('ETAT_TICKET_RESOLU', 2);

//Tableau associatif qui associe clé et valeur
$_GLOBALS['ETAT_TICKET'] =
array(
	1 => "Actif",
	0 => "Archivé",
	-1 => "Supprimé",
	2 => "Résolu"
);



