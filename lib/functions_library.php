<?php

function sanitize($str) {
	return trim(htmlentities(strip_tags($str)));
}