<?php
	require_once("header_php.php");
?>

<?php
	if (isset($_POST['send'])) {
		//Je suis dans un formulaire car le bouton send a été envoyé
		
		//Déclaration des variables temporaires
		$post_texte = "";
		$id_category = 0;
		$id_repondre_a = 0;
		
		//Assainir les données envoyées
		if (isset($_POST['texte'])) {
			$post_texte = addslashes(sanitize($_POST['texte']));
		}
		if (isset($_POST['id_category'])) {
			//On force en entier avec intval
			$id_category = intval(sanitize($_POST['id_category']));
		}
		if (isset($_POST['id_repondre_a'])) {
			//On force en entier avec intval
			$id_repondre_a = intval(sanitize($_POST['id_repondre_a']));
		}
		//On traite le cas particulier "pas de catégorie"
		if ($id_category == 0) {
			$id_category = "NULL";
		}
		//On traite le cas particulier "on ne répond pas"
		if ($id_repondre_a == 0) {
			$id_repondre_a = "NULL";
		}
		
		//Requête de création du ticket
		$dateheure = date("Y-m-d H:i:s");
		$etat = ETAT_TICKET_ACTIF;
		$id_utilisateur = $_SESSION['utilisateur_id'];
		
		$requete = "INSERT INTO ticket 
		(texte, dateheure, etat, id_utilisateur, id_categorie, id_repond_a) 
		VALUES 
		('".$post_texte."','".$dateheure."',".$etat.",".$id_utilisateur.",".$id_category.",".$id_repondre_a.")";
		
		//Execution de la requête
		$resultat = $bdd_mysql->query($requete);
		
		//Changement de page
		header("Location: board_ticket.php");
		die(); //On arrête le traitement php
	}
	
	if (isset($_GET['id_ticket'])) {
		//On répond à un ticket, on va chercher le ticket
		
		//Variables locales
		$id_ticket = 0;
		
		//Assainir les données
		$id_ticket = intval(sanitize($_GET['id_ticket']));
		
		//Ecrire la requete
		//$requete = "SELECT * FROM ticket WHERE id=".$id_ticket;
		$requete = "SELECT * FROM ticket WHERE id=?";
		
		//Executer la requete
		$stmt = $bdd_mysql->prepare($requete);
		//Lien entre le ? et la variable $id_ticket selon le type i
		$stmt->bind_param('i', $id_ticket);
		//Exécution
		$stmt->execute();
		//Récupération des résultats
		$resultat = $stmt->get_result();
		//Fermeture de la requête
		$stmt->close();
		
		if ($resultat->num_rows > 0) {
			//Il y a des lignes résultat
			$ligne_bdd = $resultat->fetch_array(MYSQLI_ASSOC);
		}
	}
?>

<?php
	require_once("header_html.php");
?>
<main>
	<?php
	if (isset($ligne_bdd)) {
		//On répond à un ticket
	?><h1>Rédigez votre réponse</h1>
	<article>
		<p><?php echo nl2br($ligne_bdd['texte']); ?></p>
	</article>
	<?php
	} else {
		//On écrit un ticket
	?><h1>Rédigez votre ticket</h1><?php
	}
	?>
	
	<form class="f_login" method="POST" action="">
		<div class="text_agauche">
			<?php include("component_category.php"); ?>
		</div>
		<div class="text_agauche">
			<textarea id="texte" name="texte"></textarea>
		</div>
		<div>
			<input type="submit" id="send" name="send" value="Send" />
		</div>
		
		<?php
		if (isset($ligne_bdd)) {
			//On répond à un ticket
		?>
		<input type="hidden" name="id_repondre_a"
		value="<?php echo $ligne_bdd['id']; ?>" />
		<?php
		}
		?>
		
	</form>
</main>
<?php
	require_once("footer_html.php");
?>





